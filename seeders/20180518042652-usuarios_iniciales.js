'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Usuarios', [
            {
                nombre: 'Sebastian',
                apellido: 'Dieguez',
                email: 'sebasoft@gmail.com',
                esAdmin: true,
                createdAt: new Date(),
                updatedAt: new Date()
            }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Usuarios', null, {});
    }
};