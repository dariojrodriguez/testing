'use strict';
module.exports = (sequelize, DataTypes) => {
    var Usuario = sequelize.define('Usuario', {
        nombre: DataTypes.STRING,
        apellido: DataTypes.STRING,
        email: DataTypes.STRING,
        esAdmin: DataTypes.BOOLEAN
    }, {});
    Usuario.associate = function (models) {
        // associations can be defined here
    };
    return Usuario;
};