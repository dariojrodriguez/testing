'use strict';
module.exports = (sequelize, DataTypes) => {
    var Mascota = sequelize.define('Mascota', {
        nombre: DataTypes.STRING,
        usuario: DataTypes.INTEGER,
        raza: DataTypes.STRING,
        sexo: DataTypes.STRING,
        activo: DataTypes.BOOLEAN
    }, {});
    Mascota.associate = function (models) {
        // associations can be defined here
    };
    return Mascota;
};