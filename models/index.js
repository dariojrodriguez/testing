'use strict';

import path from "path";
import fs from "fs";
import Sequelize from "sequelize";
const basename = path.basename(__filename);
const config = require('./../config/config.json');

const env = process.env.NODE_ENV || 'development';
const db = {};
let sequelize = null;
if (config[env].use_env_variable) {
    sequelize = new Sequelize(process.env[config[env].use_env_variable], config[env]);
} else {
    sequelize = new Sequelize(config[env].database, config[env].username, config[env].password, config[env]);
}

fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        let model = sequelize['import'](path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
export default db;
