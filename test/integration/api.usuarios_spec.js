'use strict';

let app = require('../../server'),
    chai = require('chai'),
    request = require('supertest');

let expect = chai.expect;
describe('API Usuarios', function() {
    let usuario = {
        nombre: "Sebastian",
        apellido: "Dieguez",
        email: "sebasoft@gmail.com",
        esAdmin: true
    };
    describe('# Obtener todos los uauarios', function() {
        it('Debería tener todos los usuarios', function(done) {
            request(app).get('/usuarios') .end(function(err, res) {
                expect(res.statusCode).to.equal(200);
                expect(res.body).to.be.an('array');
                expect(res.body).to.be.empty;
                done();
            });
        });
    });

    describe('## Crear un Usuario ', function() {
        it('Debería poderse crear un usuario', function(done) {
            request(app).post('/usuario') .send(usuario) .end(function(err, res) {
                expect(res.statusCode).to.equal(200);
                expect(res.body.nombre).to.equal('Sebastian');
                expect(res.body.apellido).to.equal('Dieguez');
                usuario = res.body;
                done();
            });
        });
    });
});