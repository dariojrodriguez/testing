'use strict';

import http from 'http';
import cors from 'cors';
import db from './models';
// import middleware from './middleware';
// import api from './api';

const config = require('./config/config.json');
import express from "express";
import morgan from "morgan";
import bodyParser from "body-parser";
import methodOverride from "method-override";
import finale from "finale-rest";

const env = process.env.NODE_ENV || 'development';

const app = express();
app.server = http.createServer(app);

// 3rd party middleware
app.use(cors({
    exposedHeaders: config[env].server.corsHeaders
}));

// Database
db.sequelize.authenticate()
    .then(() => {
        console.log('La conexion a la base de datos se hizo exitosamente.');
    })
    .catch(err => {
        console.error('No se pudo conectar la base de datos:', err);
    });

// Muestra cada request HTTP en la consola
app.use(morgan('dev'));
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({'extended': 'true'}));
// Limita el tamaño de subida
app.use(bodyParser.json({
    limit: config[env].server.bodyLimit
}));
// parse application/vnd.api+json as json
app.use(bodyParser.json({type: 'application/vnd.api+json'}));
app.use(methodOverride());

// API para Modelos
finale.initialize({
    app: app,
    sequelize: db.sequelize
});

//Static folder
app.use(express.static('public'));

// Usuarios
let userResource = finale.resource({
    model: db['Usuario'],
    endpoints: ['/api/usuario', '/api/usuario/:id']
});

let login = require('./routes/login');

app.use('/login', login);

// Static content
app.get('/', (req, res) => {
    res.send("GO GO PETS");
});


app.server.listen(process.env.PORT || config[env].server.port, (err) => {
    if (err) throw err;
    console.log(`Aplicacion escuchando en el puerto ${app.server.address().port}`);
});

export default app;